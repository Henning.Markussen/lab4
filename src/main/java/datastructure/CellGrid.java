package datastructure;

import cellular.CellState;

import java.util.List;

public class CellGrid implements IGrid {
    private int rows;
    private int columns;
    private CellState[][] grid;
    private CellState initialState;

    public CellGrid(int rows, int columns, CellState initialState) {
		// TODO Auto-generated constructor stub
        this.rows = rows;
        this.columns = columns;
        this.grid = new CellState[rows][columns];
        this.initialState = initialState;
        for (int i = 0; i < rows; i++){
            for (int j = 0; j< columns; j++){
                grid[i][j] = initialState;
            }
        }
	}

    @Override
    public int numRows() {
        // TODO Auto-generated method stub
        return rows;
    }

    @Override
    public int numColumns() {
        // TODO Auto-generated method stub
        return columns;
    }

    @Override
    public void set(int row, int column, CellState element) {
        // TODO Auto-generated method stub
        grid[row][column] = element;
    }

    @Override
    public CellState get(int row, int column) {
        // TODO Auto-generated method stub

        assert row<=numRows() && row >=0 && column <= numColumns() && column >=0;
        CellState state = grid[row][column];
        return state;
    }

    @Override
    public IGrid copy() {
        // TODO Auto-generated method stub
        CellGrid copyGrid = new CellGrid(numRows(), numColumns(),initialState);
        for (int i = 0; i < numRows(); i++){
            for (int j = 0; j< numColumns(); j++){
                copyGrid.set(i, j, this.get(i, j));
            }
        }
        return copyGrid;
    }
    
}
